# Proyecto básico en Flask

La estructura del proyecto será la siguiente:

```
- blueprints/
    - __init__.py
    - service.py
- test/
    - __init__.py
    - test_service.py
- app.py
- requirements.txt
```

A continuación iré abordando cada uno de los archivos incluidos en la estructura del proyecto.

## blueprints

### __init__.py

Dentro del archivo ``__init__.py`` debemos declarar todos los archivos que definan rutas o endpoints dentro del proyecto del micro servicio, algunas personas les llaman archivo **barril** a este tipo de archivos ya que importan o concentran toda la funcionalidad declarada en el directorio. En esta ocasión solo se incluirá el archivo de ``service.py``.

A continuación el código:

```
from blueprints.service import service

blueprints = [service]
```

### service.py

Dentro del archivo ``service.py`` incluiremos la funcionalidad del micro servicio, para términos practicos y demostrativos incluiremos una ruta que funcione con los métodos **HTTP** más comunes.

A continuación el código:

```python
from flask import Blueprint, request, jsonify

service = Blueprint('service', __name__)

@service.route('/', methods=['GET', 'POST', 'PUT', 'DELETE'])
def manage_service():
    if request.method == 'GET':
        return jsonify({"message": "get"})
    if request.method == 'POST':
        return jsonify({"message": "post"})
    if request.method == 'PUT':
        return jsonify({"message": "put"})
    if request.method == 'DELETE':
        return jsonify({"message": "delete"})
```

### __init__.py

El archivo ``__init__.py`` dentro de la carpeta ``test`` para este proyecto quedará vacío e inlcuiremos el código necesario de las pruebas en el archivo ``test_service.py``. Sin embargo si el micro servicio se vuelve robusto y cuenta con muchas pruebas, es posible agregar en el archivo ``__init__.py`` código base que permita por ejemplo crear la aplicación de prueba para ejecutar todos los test.

### test_service.py

Dentro del archivo ``test_service.py`` incluiremos cuatro pruebas para validar que la ruta que agregamos en el archivo ``service.py`` funciona correctamente.

A continuación el código:

```
import unittest
from app import app

class ServiceTest(unittest.TestCase):

    def setUp(self):
        self.tester = app.test_client(self)

    def test_get(self):
        response = self.tester.get('/service/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"message":"get"}\n')

    def test_post(self):
        response = self.tester.post('/service/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"message":"post"}\n')

    def test_update(self):
        response = self.tester.put('/service/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"message":"put"}\n')

    def test_delete(self):
        response = self.tester.delete('/service/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"message":"delete"}\n')
```

### app.py

Dentro del archivo ``app.py`` incluiremos el código necesario para ejecutar y poner en marcha el micro servicio, dentro de este archivo haremos referencia a los archivos descritos arriba.

A continuación el código:

```
from flask import Flask
from blueprints import blueprints

app = Flask(__name__)
for blueprint in blueprints:
    app.register_blueprint(blueprint, url_prefix="/" + str(blueprint.name))
    
if __name__ == '__main__':
    app.run('0.0.0.0', 8000)
```

### requirements.txt

Dentro del archivo ``requirements.txt`` incluiremos los paquetes y librerías necesarias para ejecutar el micro servicio, para este caso el único paquete que utilizamos es el de ``Flask``.

A continuación el código:

```
Flask==2.0.0
```