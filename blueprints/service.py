from flask import Blueprint, request, jsonify

service = Blueprint('service', __name__)

@service.route('/', methods=['GET', 'POST', 'PUT', 'DELETE'])
def manage_service():
    if request.method == 'GET':
        return jsonify({"message": "get"})
    if request.method == 'POST':
        return jsonify({"message": "post"})
    if request.method == 'PUT':
        return jsonify({"message": "put"})
    if request.method == 'DELETE':
        return jsonify({"message": "delete"})