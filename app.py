from flask import Flask
from flask.blueprints import Blueprint
from blueprints import blueprints

app = Flask(__name__)
for blueprint in blueprints:
    app.register_blueprint(blueprint, url_prefix='/' + str(blueprint.name))

if __name__ == '__main__':
    app.run('0.0.0.0', port=8000)