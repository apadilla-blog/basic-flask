import unittest
from app import app

class ServiceTest(unittest.TestCase):

    def setUp(self):
        self.tester = app.test_client(self)

    def test_get(self):
        response = self.tester.get('/service/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"message":"get"}\n')

    def test_post(self):
        response = self.tester.post('/service/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"message":"post"}\n')

    def test_update(self):
        response = self.tester.put('/service/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"message":"put"}\n')

    def test_delete(self):
        response = self.tester.delete('/service/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'{"message":"delete"}\n')